# BLUE SHARKS WEBSITE PROJECT
## HOW TO CLONE PROJECT
### Step 1

```bash
git clone https://gitlab.com/Ho-Van-Con/bs---web.git
```

### Step 2
Go to the folder where the file is installed.
```bash
cd bs---web
git pull oririgin develop
```

### Step 3
Now install the composer.
```bash
composer install
```

### Step 4
Copy the .env.example file and rename it into the .env file (For this you can run the following command).
```bash 
copy .env.example .env
```

### Step 5
Run the following command to generate a new key
```bash 
php artisan key:generate
```

### Final step
Now setup database and everything as before same.
